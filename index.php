<?php

use core\router\Router;
use core\router\Request;

/**
 * Include composer dependencies:
 */
require 'vendor/autoload.php';

/**
 * Where are you in your page and where do you go with the
 * routes and controllers
 *
 * @routes.php routes to different endpoints
 * @Request::uri get the uri
 * @Request::method POST or GET?
 */

//Start session so we can use it over the whole application
session_start();
    Router::load('routes.php')
        ->direct(Request::uri(), Request::method());
