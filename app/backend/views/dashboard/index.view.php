<?php require 'app/backend/views/partials/header.php'; ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <!-- Breadcrumbs-->
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="#">Dashboard</a>
                </li>
            </ol>
            <!-- Icon Cards-->
            <div class="row">
                <div class="col-xl-3 col-sm-6 mb-3">
                    <div class="card text-white bg-primary o-hidden h-100">
                        <div class="card-body">
                            <div class="card-body-icon">
                                <i class="fa fa-fw fa-users"></i>
                            </div>
                            <div class="mr-5"><?php echo $users; ?> Gebruikers!</div>
                        </div>
                        <a class="card-footer text-white clearfix small z-1" href="/manager/user/index">
                            <span class="float-left">Bekijk details</span>
                            <span class="float-right">
                            <i class="fa fa-angle-right"></i>
                        </span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.container-fluid-->
    </div>
<?php require 'app/backend/views/partials/footer.php'; ?>