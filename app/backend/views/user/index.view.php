<?php require 'app/backend/views/partials/header.php'; ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <!-- Breadcrumbs-->
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="#">Gebruikers</a>
                </li>
            </ol>
            <div class="row">
                <div class="col-sm-12">
                    <table class="table">
                        <thead>
                        <tr>
                            <th scope="col">Voornaam</th>
                            <th scope="col">Achternaam</th>
                            <th scope="col">Email</th>
                            <th scope="col"></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($users as $user) { ?>
                            <tr>
                                <td><?php echo $user->first_name; ?></td>
                                <td><?php echo $user->last_name; ?></td>
                                <td><?php echo $user->email; ?></td>
                                <td>
                                    <a href="/manager/user/edit/<?php echo $user->id; ?>" class="btn btn-warning">Bewerken</a>
                                    <a href="/manager/user/delete/<?php echo $user->id; ?>" class="btn btn-danger">Verwijderen</a>
                                </td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
                <div class="col-sm-12">
                    <a href="/manager/user/new" class="btn btn-success">Nieuw</a>
                </div>
            </div>
        </div>
        <!-- /.container-fluid-->
    </div>
<?php require 'app/backend/views/partials/footer.php'; ?>