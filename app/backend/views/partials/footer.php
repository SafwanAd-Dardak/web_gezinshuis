<footer class="sticky-footer">
    <div class="container">
        <div class="text-center">
            <small>&copy; <?php echo $year; ?> - <a href="/">Gezinshuis Regterink
            </small>
        </div>
    </div>
</footer>
<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fa fa-angle-up"></i>
</a>

<!-- Bootstrap core JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0/js/bootstrap.bundle.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>

<script src="/public/backend/js/datatables/jquery.dataTables.js"></script>
<script src="/public/backend/js/datatables/dataTables.bootstrap4.js"></script>

<script src="/public/backend/js/sb-admin.js"></script>
<script src="/public/backend/js/alert.js"></script>
</body>
</html>