<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <meta name="robots" content="noindex">
    <meta name="googlebot" content="noindex">

    <!-- Title -->
    <title>Gezinshuis Regterink - CMS</title>

    <!-- CSS Files -->
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.19/css/dataTables.bootstrap4.min.css">

    <!-- Custom styles -->
    <link rel="stylesheet" href="/public/backend/css/sb-admin.css">
    <link rel="stylesheet" href="/public/backend/css/custom.css">

    <!-- favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="/public/frontend/images/favicon.ico">

    <!-- Script -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</head>
<body>
<?php include "menu.php"; ?>