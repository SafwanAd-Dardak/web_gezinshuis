<?php
/**
 * Created by PhpStorm.
 * User: safwan
 * Date: 20/10/2018
 * Time: 22:13
 */


namespace app\backend\controllers;

use core\models\User;

/**
 * Class IndexController
 * @package app\controllers
 */
class UserController extends ControllerBase
{
    /**
     * Function for the index of the website
     */
    public function index()
    {
        // Get the general data
        list($year) = $this->initialize();
        // Create a new user object
        $userObject = new User();
        // Ge the users
        $users = $userObject->find($userObject->getSource(), User::class);

        // Require the view
        require 'app/backend/views/user/index.view.php';
    }

    /**
     * Function for the index of the website
     * @param $id
     */
    public function create()
    {
        // Get the general data
        list($year) = $this->initialize();
        // Create a new user object
        $userObject = new User();
        // Declare the keys string
        $keys = 'first_name, last_name, nickname, birth_date, email, password, phone_number, picture, reason, proficiency, active, role';
        // Declare the keys string
        $placeholders = ':first_name, :last_name, :nickname, :birth_date, :email, :password, :phone_number, :picture, :reason, :proficiency, :active, :role';
        // Declare the values array
        $values = [
            ':first_name' => $_POST['first_name'],
            ':last_name' => $_POST['last_name'],
            ':nickname' => $_POST['nickname'],
            ':birth_date' => $_POST['birth_date'],
            ':email' => $_POST['email'],
            ':password' => $_POST['password'],
            ':phone_number' => $_POST['phone_number'],
            ':picture' => $_POST['picture'],
            ':reason' => $_POST['reason'],
            ':proficiency' => $_POST['proficiency'],
            ':active' => $_POST['active'],
            ':role' => $_POST['role'],
        ];

        // Ge the users
        $user = $userObject->insert($keys, $placeholders, $values, $userObject->getSource());

        // Require the view
        require 'app/backend/views/user/index.view.php';
    }

    /**
     * Function for the index of the website
     * @param $id
     */
    public function edit($id)
    {
        // Get the general data
        list($year) = $this->initialize();
        // Create a new user object
        $userObject = new User();
        // Ge the users
        $user = $userObject->findBy('id', $id, $userObject->getSource(), User::class);

        // Require the view
        require 'app/backend/views/user/index.view.php';
    }

    /**
     * Function for the index of the website
     * @param $id
     */
    public function delete($id)
    {
        // Create a new user object
        $userObject = new User();
        // Ge the users
        $user = $userObject->delete($id, $userObject->getSource());
    }
}