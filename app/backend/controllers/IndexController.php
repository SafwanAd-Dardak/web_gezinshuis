<?php
/**
 * Created by PhpStorm.
 * User: safwan
 * Date: 20/10/2018
 * Time: 22:13
 */


namespace app\backend\controllers;

use core\models\User;

/**
 * Class IndexController
 * @package app\controllers
 */
class IndexController extends ControllerBase
{
    /**
     * Function for the index of the website
     */
    public function index() {
        // Get the general data
        list($year) = $this->initialize();

        // Create a new user object
        $userObject = new User();
        // Ge the users
        $users = count($userObject->find($userObject->getSource(), User::class));

        // Require the view
        require 'app/backend/views/dashboard/index.view.php';
    }
}