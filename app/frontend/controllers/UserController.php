<?php

namespace app\frontend\controllers;

use core\models\User;

/**
 * Class UserController
 * @package app\controllers
 */
class UserController extends ControllerBase
{

    /**
     * /login GET method - Login page
     */
    public function login()
    {
        //check if user is logged in
        if (isset($_SESSION['user'])) {
            header('Location: /login-redirect');
        }

        require 'app/frontend/views/account/login.view.php';
    }

    /**
     * /login-check POST method - Authenticate user
     */
    public function loginPost()
    {
        $email = isset($_POST['email']) ? $_POST['email'] : null;
        $password = isset($_POST['password']) ? $_POST['password'] : null;

        $user = new User();
        $result = $user->findBy('email', $email, $user->getSource(), User::class);

        //Verify if entered password is equal to hash
        if (password_verify($password, $result->password)) {
            $_SESSION['user'] = $result->id;

            header('Location: /login-redirect');
            return;
        }

        //Set error_login on true. In the view it will check for it and show an error
        $error_login = true;
        require 'app/frontend/views/account/login.view.php';
    }

    /**
     * /login-redirect GET method - redirect to user's role dashboard page
     */
    public function loginRedirect()
    {
        if (isset($_SESSION['user'])) {
            $userId = $_SESSION['user'];

            $user = new User();
            $result = $user->findBy('id', $userId, $user->getSource(), User::class);

            //Redirect user with certain role to the right page
            switch ($result->role) {
                case User::OWNER:
                    header('Location: /manager/dashboard');
                    return;
                    break;
                case User::DOCTOR:
                    //TODO: change this page to a real one
                    header('Location: /doctor-page');
                    return;
                    break;
                case User::PARENT:
                    //TODO: change this page to a real one
                    header('Location: /parent-page');
                    return;
                    break;
                case User::KID:
                    //TODO: change this page to a real one
                    header('Location: /kid-page');
                    return;
                    break;
            }
        }

        header('Location: /');
    }

    /**
     * /profile GET method - Showing profile page
     */
    public function profile()
    {
        //Check in session if user is logged in
        if (!isset($_SESSION['user'])) {
            header('Location: /login-redirect');
        }

        require 'app/frontend/views/account/profile.view.php';

    }

    /**
     * /change-password POST method - Change password of user
     */
    public function changePasswordPost()
    {
        if (!isset($_SESSION['user'])) {
            echo '{"status":"not-logged-in"}';
            exit;
        }

        //Retrieve variables from POST
        $password = isset($_POST['password']) ? $_POST['password'] : null;
        $passwordRepeat = isset($_POST['passwordRepeat']) ? $_POST['passwordRepeat'] : null;

        //check if passwords are equal
        if ($password != $passwordRepeat) {
            echo '{"status":"password-incorrect"}';
            exit;
        }

        //Call method to change password
        $user = new User();
        $user->changePassword($_SESSION['user'], $password);

        echo '{"status":"success"}';
        exit;
    }

    /**
     * /logout GET method - Logout user
     */
    public function logout()
    {
        session_destroy();
        header('Location: /');
    }
}