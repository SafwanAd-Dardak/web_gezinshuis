<?php
/**
 * Created by PhpStorm.
 * User: safwan
 * Date: 20/10/2018
 * Time: 22:13
 */


namespace app\frontend\controllers;

use core\models\User;

/**
 * Class IndexController
 * @package app\controllers
 */
class IndexController extends ControllerBase
{
    /**
     * Function for the index of the website
     */
    public function index() {
        // Get the general data
        list($currentUri, $year) = $this->initialize();

        // Create a new object of the user class
        $user = new User();
        // Get all the users
        $results = $user->find($user->getSource(), User::class);
        // Get an user by id
        $result = $user->findBy('id', 1, $user->getSource(), User::class);

        // Declare the title
        $title = 'Home';
        // Require the view
        require 'app/frontend/views/index/index.view.php';
    }
}