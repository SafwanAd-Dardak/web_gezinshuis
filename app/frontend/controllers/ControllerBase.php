<?php
/**
 * Created by PhpStorm.
 * User: safwan
 * Date: 20/10/2018
 * Time: 22:13
 */


namespace app\frontend\controllers;

use core\models\User;
use core\router\Request;

/**
 * Class ControllerBase
 * @package app\controllers
 */
class ControllerBase
{
    /**
     * Function to get general data
     * @return array
     */
    public function initialize() {
        // Get current uri
        $currentUri = Request::uri();
        // Get the current year
        $year = date('Y');

        // Return the data
        return [$currentUri, $year];
    }

    /**
     * @param $user_role
     * Function that checks if the user has access to an method
     */
    protected function authorize($user_role) {
        if (isset($_SESSION['user'])) {
            $user       = new User();
            $result     = $user->findBy('id', $_SESSION['user'], $user->getSource(), User::class);

            //If user is not found for some reason then redirect to homepage
            if ($result == null) {
                header('Location: /');
                return;
            }

            //If user is not the role that it should be then redirect to the roles homepage
            if ($result->role != $user_role) {
                header('Location: /login-redirect');
                return;
            }

            return true;
        }

        //User is not logged in so redirect to homepage
        header('Location: /');
    }
}