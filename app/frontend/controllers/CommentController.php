<?php
/**
 * Created by PhpStorm.
 * User: safwan
 * Date: 20/10/2018
 * Time: 22:13
 */


namespace app\frontend\controllers;

use core\models\CarePlan;
use core\models\Comment;
use core\models\Event;
use core\models\User;

/**
 * Class IndexController
 * @package app\controllers
 */
class CommentController extends ControllerBase
{

    private function authorizeComments() {
        if (!isset($_SESSION['user'])) {
            echo '{"result":"error"}';exit;
        }

        $user       = new User();
        $user     = $user->findBy('id', $_SESSION['user'], $user->getSource(), User::class);

        if ($user->role == User::KID) {
            echo '{"result":"error"}';exit;
        }

        $table  = isset($_GET['table']) ? $_GET['table'] : null;
        $id     = isset($_GET['id']) ? $_GET['id'] : null;

        $possible_tables = ['event', 'care_plan'];
        if (!in_array($table, $possible_tables)) {
            echo '{"result":"error"}';exit;
        }

        if ($table == 'event') {
            $event = new Event();
            $event = $event->findBy('id', $id, $event->getSource(), Event::class);

            if ($event == null) {
                echo '{"result":"error"}';exit;
            }
        }

        if ($table == 'care_plan') {
            $care_plan = new CarePlan();
            $care_plan = $care_plan->findBy('id', $id, $care_plan->getSource(), CarePlan::class);
            if ($care_plan == null) {
                echo '{"result":"error"}';exit;
            }
        }


        if($user->role == User::PARENT) {
            if ($table == 'event') {
                $creator_id = $event->creator_id;
                if ($creator_id != $user->id) {
                    echo '{"result":"error"}';exit;
                }
            }

            if ($table == 'care_plan') {
                $kid = $care_plan->getKid();

                if ($kid->parent_id != $user->id) {
                    echo '{"result":"error"}';exit;
                }
            }
        }
    }

    /**
     * Function for the index of the website
     */
    public function getComments() {
        $this->authorizeComments();

        $table  = isset($_GET['table']) ? $_GET['table'] : null;
        $id     = isset($_GET['id']) ? $_GET['id'] : null;

        $comment       = new Comment();
        $result        = $comment->findMultipleBy($table . '_id', $id, $comment->getSource(), Comment::class);

        if (empty($result)) {
            $result = [];
        }

        echo json_encode($result);exit;
    }

    public function addComment() {
        $this->authorizeComments();
        $table  = isset($_POST['table']) ? $_POST['table'] : null;
        $id     = isset($_POST['id']) ? $_POST['id'] : null;
        $comment     = isset($_POST['comment']) ? $_POST['comment'] : null;
        if (empty($comment)) {
            echo '{"result":"error"}';exit;
        }

        $user       = new User();
        $user     = $user->findBy('id', $_SESSION['user'], $user->getSource(), User::class);

        $commentEntity = new Comment();
        $commentEntity->comment = $comment;
        $commentEntity->created_by = $user->id;
        $commentEntity->votes = 0;
        $commentEntity->{$table . '_id'} = $id;
        $commentEntity->save();

        echo json_encode((object)['result' => 'success']);exit;

    }
}