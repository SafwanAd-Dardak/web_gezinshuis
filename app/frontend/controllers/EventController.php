<?php
/**
 * Created by PhpStorm.
 * User: safwan
 * Date: 20/10/2018
 * Time: 22:13
 */


namespace app\frontend\controllers;

use core\models\Event;
use core\models\User;

/**
 * Class IndexController
 * @package app\controllers
 */
class EventController extends ControllerBase
{
    /**
     * Function for the index of the website
     */
    public function index() {
        if (!isset($_SESSION['user'])) {
            echo '{"result":"error"}';exit;
        }

        $user       = new User();
        $user     = $user->findBy('id', $_SESSION['user'], $user->getSource(), User::class);

        $events = new Event();
        $events = $events->findMultipleBy('creator_id', $user->id, $events->getSource(), User::class);

        require 'app/frontend/views/events/index.view.php';
    }

    public function addEvent() {
        if (!isset($_SESSION['user'])) {
            echo '{"result":"error"}';exit;
        }

        $user       = new User();
        $user     = $user->findBy('id', $_SESSION['user'], $user->getSource(), User::class);

        require 'app/frontend/views/events/create.view.php';

    }

    public function addEventPost() {
        if (!isset($_SESSION['user'])) {
            echo '{"result":"error"}';exit;
        }

        $user       = new User();
        $user     = $user->findBy('id', $_SESSION['user'], $user->getSource(), User::class);

        $event_name = isset($_POST['event_name']) ? $_POST['event_name'] : null;
        $image      = isset($_FILES['image']) ? $_FILES['image'] : null;

        if (empty($event_name) || empty($image)) {
            header('Location: /event/create');return;
        }

        $picture_hash = null;
        if ($image != null && $image['error'] == 0) {
            $picture_hash = date('YmdHis') . '.png'; //TODO: I know
            move_uploaded_file($image['tmp_name'], 'var/images/' . $picture_hash);
        }

        $event = new Event();
        $event->creator_id = $user->id;
        $event->date_event = date('Y-m-d');
        $event->event_name = $event_name;
        $event->picture = $picture_hash;

        $event->save();

        header('Location: /event/show?id=' . $event->id);
    }

    public function show() {
        if (!isset($_SESSION['user'])) {
            echo '{"result":"error"}';exit;
        }

        $user       = new User();
        $user     = $user->findBy('id', $_SESSION['user'], $user->getSource(), User::class);

        $event = new Event();
        $event     = $event->findBy('id', $_GET['id'], $event->getSource(), Event::class);
        if ($event == null || $event->creator_id != $user->id) {
            header('Location: /events');return;
        }

        require 'app/frontend/views/events/show.view.php';

    }

    public function showImage() {
        if (!isset($_SESSION['user'])) {
            echo '{"result":"error"}';exit;
        }

        $user       = new User();
        $user     = $user->findBy('id', $_SESSION['user'], $user->getSource(), User::class);

        $event = new Event();
        $event     = $event->findBy('id', $_GET['id'], $event->getSource(), Event::class);

        if ($event == null || $event->creator_id != $user->id) {
            header('Location: /events');return;
        }

        if (file_exists('var/images/' . $event->picture)) {

            //Set the content-type header as appropriate
            $image_info = getimagesize('var/images/' . $event->picture);
            switch ($image_info[2]) {
                case IMAGETYPE_JPEG:
                    header("Content-Type: image/jpeg");
                    break;
                case IMAGETYPE_GIF:
                    header("Content-Type: image/gif");
                    break;
                case IMAGETYPE_PNG:
                    header("Content-Type: image/png");
                    break;
                default:
                    header($_SERVER["SERVER_PROTOCOL"] . " 500 Internal Server Error");
                    break;
            }

            // Set the content-length header
            header('Content-Length: ' . filesize('var/images/' . $event->picture));

            // Write the image bytes to the client
            readfile('var/images/' . $event->picture);
        }

    }


}