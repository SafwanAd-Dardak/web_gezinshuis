<?php
/**
 * Created by PhpStorm.
 * User: safwan
 * Date: 20/10/2018
 * Time: 22:13
 */


namespace app\frontend\controllers;

/**
 * Class IndexController
 * @package app\controllers
 */
class ContactController extends ControllerBase
{
    /**
     * Function for the index of the website
     */
    public function index() {
        // Get the general data
        list($currentUri, $year) = $this->initialize();

        // Declare the title
        $title = 'Contact';
        // Require the view
        require 'app/frontend/views/contact/index.view.php';
    }

    /**
     * Function for the index of the website
     */
    public function sendMail() {
        // TODO: Edit $to to necessary mailaddress

        $missing[] = "";
        $count = 0;
        foreach($_POST as $key => $value){
            if(empty($value)){
                $missing[$count] = $key;
                $count += 1;
            }
        }
        if($count != 0){
            $_SESSION["msg"]["info"] = "U heeft ";
            $amountMissing = count($missing);
            foreach($missing as $value){
                $_SESSION["msg"]["info"] .= ucfirst($value);
                --$amountMissing;
                if($amountMissing == 1){
                    $_SESSION["msg"]["info"] .= " en ";
                }elseif($amountMissing > 0){
                    $_SESSION["msg"]["info"] .= ", ";
                }
            }
            $_SESSION["msg"]["info"] .= " niet ingevuld.";
        }else{
            $to = "jasper.folkers@gmail.com";
            $headers =
                "From:admin@joeymariah.me" . "\r\n" .
                "Reply-To: " . $_POST["email"] . "\r\n" .
                "X-Mailer: PHP/" . phpversion();
            $subject = $_POST["onderwerp"];
            $msg = wordwrap($_POST["inhoud"]);
            $mail=mail($to, $subject, $msg, $headers);
            if($mail){
                $_SESSION["msg"]["success"] = "Uw vraag is succesvol verstuurd";
            }else{
                $_SESSION["msg"]["danger"] = "Er is iets fout gegaan, probeert u het nogmaals. ";
                $_SESSION["msg"]["danger"] .= "Blijft dit probleem zich voordoen, neem dan contact op met de administrator van de site.";
            }
        }



        header('Location: /contact');
    }
}