<?php require 'app/frontend/views/partials/header.php'; ?>
    <!-- Page Content -->
    <div class="container h-100">
        <div class="row h-100 align-items-center">
            <div class="col-2"></div>
            <div class="col-8">
                <form class="form-signin shadow" method="post" action="post-event" enctype="multipart/form-data">
                    <h2 class="form-signin-heading">Maak event</h2>
                    <label for="inputPassword" class="sr-only">Naam</label>
                    <input type="text" id="inputPassword" name="event_name" class="form-control" placeholder="Naam"
                           required="" autofocus="" autocomplete="off">
                    <label for="inputPasswordRepeat" class="sr-only">Afbeelding</label>
                    <p>Afbeelding</p>
                    <input type="file" id="image" name="image" class="form-control" required="" autocomplete="off">
                    <button class="btn btn-lg btn-primary btn-block" type="submit" id="createEvent">Maak event</button>
                    <p id="status-text" class="hidden"></p>
                </form>
            </div>
            <div class="col-2"></div>
        </div>
    </div>
    <!-- /.container -->
<?php require 'app/frontend/views/partials/footer.php'; ?>