<?php require 'app/frontend/views/partials/header.php'; ?>
    <!-- Page Content -->
    <div class="container h-100">
        <div class="row h-100 align-items-center">
            <div class="col-2"></div>
            <div class="col-8">
                <h1>Event - <?php echo $event->event_name; ?></h1>
                <img src="image?id=<?php echo $event->id; ?>" width="500" height="auto">
                <div id="comments" data-id="<?php echo $event->id ?>" data-table="event"></div>
            </div>
            <div class="col-2"></div>
        </div>
    </div>
    <!-- /.container -->
<?php require 'app/frontend/views/partials/footer.php'; ?>