<?php require 'app/frontend/views/partials/header.php'; ?>
    <!-- Page Content -->
    <div class="container h-100">
        <div class="row h-100 align-items-center">
            <div class="col-2"></div>
            <div class="col-8">
                <h1>Events</h1>
                <a href="create-event">Nieuw</a>
                <table>
                    <thead>
                    <tr>
                        <th>Id</th>
                        <th>Naam</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach($events as $event) { ?>
                        <tr>
                            <td><?php echo $event->id; ?></td>
                            <td><?php echo $event->event_name; ?></td>
                            <td><a href="event/show?id=<?php echo $event->id; ?>">Zie</a></td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
            <div class="col-2"></div>
        </div>
    </div>
    <!-- /.container -->
<?php require 'app/frontend/views/partials/footer.php'; ?>