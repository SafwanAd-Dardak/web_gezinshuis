<?php require 'app/frontend/views/partials/header.php'; ?>
    <!-- Page Content -->
    <div class="container">
        <div class="row">
            <div class="col-1"></div>
            <div class="col-10">
                <div class="card shadow my-4">
                    <div class="card-header">
                        <h2 class="card-title">Neem contact met ons op!</h2>
                    </div>
                    <div class="card-block">
                        <p class="mx-4 my-3">
                            Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.
                            Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
                            Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.
                            Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo.
                            Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus.
                            Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus.
                            Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui.
                        </p>
                    </div>
                    <div class="card-footer">
                        <form method="post" action="send-mail">
                            <div class="form-group">
                                <label for="inputEmail" class="sr-only">Emailadres</label>
                                <input type="email" id="inputEmail" name="email" class="form-control" placeholder="Emailadres" required="" autofocus="" autocomplete="off">
                            </div>
                            <div class="form-group">
                                <label for="inputSubject" class="sr-only">Onderwerp</label>
                                <input type="text" id="inputSubject" name="onderwerp" class="form-control" placeholder="Onderwerp" required="" autocomplete="off">
                            </div>
                            <div class="form-group">
                                <label for="textfield"  class="sr-only">Inhoud</label>
                                <textarea rows="8" placeholder="Vul hier uw vraag in..." id="textfield" name="inhoud" required="" class="form-control"></textarea>
                            </div>
                            <button class="btn btn-lg btn-primary btn-block" type="submit">Verstuur</button>
                        </form>
                    </div>
                </div>

            </div>
            <div class="col-1"></div>
        </div>

    </div>
    <!-- /.container -->
<?php require 'app/frontend/views/partials/footer.php'; ?>