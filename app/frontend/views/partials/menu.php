<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-light bg-light rounded-bottom shadow">
    <div class="container">
        <a class="navbar-brand" href="/">
            <img src="/public/frontend/images/logos/Gezinshuis_Regterink_logo_compact.png" width="150" height="50" alt="Gezinshuis Regterink logo">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item <?php echo $currentUri === "/" ? 'active' : ''; ?>">
                    <a class="nav-link" href="/">Home</a>
                </li>
                <li class="nav-item <?php echo $currentUri === "/contact" ? 'active' : ''; ?>">
                    <a class="nav-link" href="/contact">Contact</a>
                </li>

                <?php
                    if(isset($_SESSION["user"])){
                        echo "<li class=\"nav-item";
                        echo $currentUri === "/profile" ? 'active' : '';
                        echo "\">";
                        echo "<a class=\"nav-link\" href=\"/profile\">Profiel</a>";
                        echo "</li>";
                    }

                ?>

            </ul>
            <ul class="navbar-nav ml-auto">
                <?php
                if(isset($_SESSION["user"])){
                    echo "<li class='nav-item";
                    echo $currentUri === "/logout" ? 'active' : '';
                    echo "'>";
                    echo "<a class=\"nav-link\" href=\"/logout\"><span class=\"fas fa-sign-out-alt\"></span> Logout</a>";
                    echo "</li>";
                }
                ?>
            </ul>
        </div>
    </div>
</nav>