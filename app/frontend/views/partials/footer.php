<footer class="footer rounded-top shadow">
    <div class="container text-center">
        <span class="text-muted">&copy; <?php echo $year; ?> - <a href="/">Gezinshuis Regterink</a></span>
    </div>
</footer>
<!-- Bootstrap core JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.bundle.min.js"></script>
<script src="/public/frontend/js/script.js"></script>
</body>
</html>