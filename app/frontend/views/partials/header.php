<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Title of the page -->
    <title><?php echo isset($title) ? $title . ' - ' : ''; ?> Gezinshuis Regterink</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">

    <!-- Custom styles -->
    <link rel="stylesheet" href="/public/frontend/css/sticky-footer.css">
    <link rel="stylesheet" href="/public/frontend/css/login-page.css">
    <link rel="stylesheet" href="/public/frontend/css/style.css"/>

    <!-- favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="/public/frontend/images/favicon.ico">

    <!-- Script -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</head>
<body>
<?php
include "menu.php";

if(isset($_SESSION["msg"])){
    echo "<div class=\"mt-2\"></div>";
    foreach($_SESSION["msg"] as $key => $value){
        echo "<div class=\"alert alert-$key\" role=\"alert\">";
        echo $value;
        echo "</div>";
    }
    unset($_SESSION["msg"]);
}
?>


