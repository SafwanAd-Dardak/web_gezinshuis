<?php require 'app/frontend/views/partials/header.php'; ?>
    <!-- Page Content -->
    <div class="container h-100">
        <div class="row mt-3">
            <div class="col-1"></div>
            <div class="col-10">
                <div id="carouselExampleIndicators" class="carousel shadow slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                    </ol>
                    <div class="carousel-inner rounded">
                        <div class="carousel-item active">
                            <img class="carousel-center d-block w-100" src="/public/frontend/images/Carrousel/slide1.jpg" alt="First slide">
                            <div class="carousel-caption d-none d-md-block">
                                <h5>Slide 1 Label</h5>
                                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</p>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="/public/frontend/images/Carrousel/slide2.jpg" alt="Second slide">
                            <div class="carousel-caption d-none d-md-block">
                                <h5>Slide 2 Label</h5>
                                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</p>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="/public/frontend/images/Carrousel/slide3.jpg" alt="Third slide">
                            <div class="carousel-caption d-none d-md-block">
                                <h5>Slide 3 Label</h5>
                                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</p>
                            </div>
                        </div>
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
            <div class="col-1"></div>
        </div>
        <div class="row mb-3 mt-4">
            <div class="col-1"></div>
            <div class="col-10">
                <div class="card shadow">
                    <div class="card-header">
                        <h2 class="card-title">Gezinshuis Regterink</h2>
                    </div>
                    <div class="card-block">
                        <p class="mx-4 my-3">
                            Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.
                            Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
                            Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.
                            Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo.
                            Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus.
                            Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus.
                            Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui.
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-1"></div>
        </div>
    </div>
    <!-- /.container -->
<?php require 'app/frontend/views/partials/footer.php'; ?>