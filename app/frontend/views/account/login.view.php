<?php require 'app/frontend/views/partials/header.php'; ?>
    <!-- Page Content -->
    <div class="container h-100">
        <div class="row h-100 align-items-center">
            <div class="col-4"></div>
            <div class="col-4">
                <form class="form-signin shadow" method="post" action="login-check">
                    <h2 class="form-signin-heading">Inloggen</h2>
                    <label for="inputEmail" class="sr-only">Emailadres</label>
                    <input type="email" id="inputEmail" name="email" class="form-control" placeholder="Emailadres" required="" autofocus="" autocomplete="off">
                    <label for="inputPassword" class="sr-only">Wachtwoord</label>
                    <input type="password" id="inputPassword" name="password" class="form-control" placeholder="Wachtwoord" required="" autocomplete="off">
                    <?php if (isset($error_login)) { ?><p style="color: red;">Email/wachtwoord onjuist</p><?php } ?>
                    <button class="btn btn-lg btn-primary btn-block" type="submit">Inloggen</button>
                </form>
            </div>
            <div class="col-4"></div>
        </div>


    </div>
    <!-- /.container -->
<?php require 'app/frontend/views/partials/footer.php'; ?>