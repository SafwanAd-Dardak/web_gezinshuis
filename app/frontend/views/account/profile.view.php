<?php require 'app/frontend/views/partials/header.php'; ?>
    <!-- Page Content -->
    <div class="container h-100">
        <div class="row h-100 align-items-center">
            <div class="col-2"></div>
            <div class="col-8">
                <form class="form-signin shadow" method="post" action="change-passord">
                    <h2 class="form-signin-heading">Verander uw wachtwoord</h2>
                    <label for="inputPassword" class="sr-only">Wachtwoord</label>
                    <input type="email" id="inputPassword" name="password" class="form-control" placeholder="Wachtwoord"
                           required="" autofocus="" autocomplete="off">
                    <label for="inputPasswordRepeat" class="sr-only">Herhaal uw wachtwoord</label>
                    <input type="password" id="inputPasswordRepeat" name="password-repeat" class="form-control"
                           placeholder="Herhaal wachtwoord" required="" autocomplete="off">
                    <button class="btn btn-lg btn-primary btn-block" type="submit" id="changePassword">Wijzig
                        wachtwoord
                    </button>
                    <p id="status-text" class="hidden"></p>
                </form>
            </div>
            <div class="col-2"></div>
        </div>
    </div>
    <!-- /.container -->
<?php require 'app/frontend/views/partials/footer.php'; ?>