<?php
/**
 * Created by PhpStorm.
 * User: safwan
 * Date: 20/10/2018
 * Time: 22:13
 */


namespace core\router;

/**
 * Class Request
 */
class Request
{
    /**
     * Get the request_uri
     * @return mixed
     */
    public static function uri()
    {
        return trim($_SERVER['REQUEST_URI']);
    }

    /**
     * GET of POST method
     * @return mixed
     */
    public static function method()
    {
        return $_SERVER['REQUEST_METHOD'];
    }
}