<?php
/**
 * GET routes
 */
$router->get('/', 'app\frontend\controllers\IndexController', 'index');
$router->get('/contact', 'app\frontend\controllers\ContactController', 'index');

//Login Routes (Should I move these actions to backend?)
$router->get('/login', 'app\frontend\controllers\UserController', 'login');
$router->get('/login-redirect', 'app\frontend\controllers\UserController', 'loginRedirect');
$router->get('/logout', 'app\frontend\controllers\UserController', 'logout');
$router->get('/profile', 'app\frontend\controllers\UserController', 'profile');

//Comments Routes
$router->get('/comments', 'app\frontend\controllers\CommentController', 'getComments');

//Events Routes
$router->get('/events', 'app\frontend\controllers\EventController', 'index');
$router->get('/event/show', 'app\frontend\controllers\EventController', 'show');
$router->get('/create-event', 'app\frontend\controllers\EventController', 'addEvent');
$router->get('/event/image', 'app\frontend\controllers\EventController', 'showImage');


$router->get('/manager/dashboard', 'app\backend\controllers\IndexController', 'index');
$router->get('/manager/user/index', 'app\backend\controllers\UserController', 'index');
$router->get('/manager/user/new', 'app\backend\controllers\UserController', 'create');
$router->get('/manager/user/edit/{id}', 'app\backend\controllers\UserController', 'edit');
$router->get('/manager/user/delete/{id}', 'app\backend\controllers\UserController', 'delete');

/**
 * POST routes
 */
$router->post('/login-check', 'app\frontend\controllers\UserController', 'loginPost');
$router->post('/change-password', 'app\frontend\controllers\UserController', 'changePasswordPost');
$router->post('/post-comment', 'app\frontend\controllers\CommentController', 'addComment');
$router->post('/post-event', 'app\frontend\controllers\EventController', 'addEventPost');