<?php
/**
 * Created by PhpStorm.
 * User: safwan
 * Date: 20/10/2018
 * Time: 22:13
 */

namespace core\router;

/**
 * Class Router
 * @package core\router
 */
class Router
{
    /**
     * @var array
     */
    private $routes = [
        'GET' => [],
        'POST' => [],
        'PUT' => [],
        'DELETE' => []
    ];

    /**
     * Function to load the routes.php file
     * @param $file
     * @return Router
     */
    public static function load($file)
    {
        $router = new self();
        require $file;
        return $router;
    }

    /**
     * Function to create the get routes
     * @param $uri
     * @param $controller
     * @param $method
     */
    public function get($uri, $controller, $method)
    {
        // Get the params form the handleUri function
        list($uri, $params) = $this->handleUri($uri);

        // Set the params
        $this->routes['GET'][$uri] = [
            'controller' => $controller,
            'method' => $method,
            'params' => $params
        ];
    }

    /**
     * Function to create the POST routes
     * @param $uri
     * @param $controller
     * @param $method
     */
    public function post($uri, $controller, $method)
    {
        // Get the params form the handleUri function
        list($uri, $params) = $this->handleUri($uri);

        // Set the params
        $this->routes['POST'][$uri] = [
            'controller' => $controller,
            'method' => $method,
            'params' => $params
        ];
    }

    /**
     * Function to create the PUT routes
     * @param $uri
     * @param $controller
     * @param $method
     */
    public function put($uri, $controller, $method)
    {
        // Get the params form the handleUri function
        list($uri, $params) = $this->handleUri($uri);

        // Set the params
        $this->routes['PUT'][$uri] = [
            'controller' => $controller,
            'method' => $method,
            'params' => $params
        ];
    }

    /**
     * Function to create the DELETE routes
     * @param $uri
     * @param $controller
     * @param $method
     */
    public function delete($uri, $controller, $method)
    {
        // Get the params form the handleUri function
        list($uri, $params) = $this->handleUri($uri);

        // Set the params
        $this->routes['DELETE'][$uri] = [
            'controller' => $controller,
            'method' => $method,
            'params' => $params
        ];
    }

    /**
     * Function to check if the route exists and call the call method
     * @param $uri
     * @param $requestType
     * @throws \Exception
     */
    public function direct($uri, $requestType)
    {
        $uri = $this->removeQueryStringVariables($uri);
        // Loop over the routes
        foreach ($this->routes[$requestType] as $key => $route) {
            // Check fi url matches one of the routes
            if (strpos($uri, $key) !== false) {
                // Get the params and make an array of the params
                $paramsArray = explode('/', str_replace($key, '', $uri));
                $paramsArray = array_filter($paramsArray, function($value) { return $value !== ''; });
                // Check if the amount of params is the same
                if (count($paramsArray) === count($route['params'])) {
                    // Call the set function to call the controller and method with its params
                    $this->set($route['controller'], $route['method'], $paramsArray);
                    return;
                }
            }
        }
        throw new \Exception('Sorry uri not defined!');
    }

    /**
     * Function to call the controller and method
     * @param $controller
     * @param $method
     * @param $params
     * @return mixed
     */
    private function set($controller, $method, $params)
    {
        // Call method on you controller object
        return call_user_func_array(
            array(new $controller(), $method), $params
        );
    }

    /**
     * Function to handle the Uri with params
     * @param $uri
     * @return array
     */
    private function handleUri($uri)
    {
        // Get all the params
        preg_match_all('~{[^{}]*}~', $uri, $matches);
        // Loop over the params
        foreach ($matches[0] as $match) {
            // Remove the params form the url
            $uri = str_replace($match, '', $uri);
        }
        // Remove slashes that are not needed
        $uri = preg_replace('/(\/+)/', '/', $uri);
        // Return the data

        return [$uri, $matches[0]];
    }

    protected function removeQueryStringVariables($url) {
        if ($url != '') {
            $url = str_replace('?', '&', $url);

            //Seperate variables in URL
            $parts = explode('&', $url, 2);

            //Retrieve normal URL
            if (strpos($parts[0], '=') === false) {
                $url = $parts[0];
            } else {
                $url = '';
            }
        }

        return $url;
    }
}