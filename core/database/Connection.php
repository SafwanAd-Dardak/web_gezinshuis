<?php
/**
 * Created by PhpStorm.
 * User: safwan
 * Date: 20/10/2018
 * Time: 22:13
 */


namespace core\database;

/**
 * Class Connection
 */
class Connection
{
    /**
     * Function to create PDO connection with DB
     * @param $config
     * @return \PDO
     */
    public static function make($config)
    {
        try {
            return new \PDO(
                $config['connection'] . ';dbname=' . $config['name'],
                $config['username'],
                $config['password'],
                $config['options']
            );
        } catch (\PDOException $e) {
            die($e->getMessage());
        }
    }
}