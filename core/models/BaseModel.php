<?php
/**
 * Created by PhpStorm.
 * User: safwan
 * Date: 20/10/2018
 * Time: 22:13
 */


namespace core\models;

use core\database\Connection;

/**
 * Class BaseModel
 * @package app\models
 */
class BaseModel
{
    /**
     * @var \PDO
     */
    protected $pdo;

    /**
     * Function to make the pdo connection
     * Model constructor.
     */
    public function __construct()
    {
        // Require the config file
        $app = require 'core/config/config.php';
        // Declare the pdo connection
        $this->pdo = Connection::make($app['database']);
    }

    /**
     * Function to find all records of table
     * @param $table
     * @param $intoClass
     * @return array
     */
    public function find($table, $intoClass)
    {
        // Prepare the sql statement
        $statement = $this->pdo->prepare("select * from {$table}");
        // Execute the
        $statement->execute();

        return $statement->fetchAll(\PDO::FETCH_CLASS, $intoClass);
    }

    /**
     * Function to find row by column and value
     * @param $table
     * @param $column
     * @param $value
     * @param $intoClass
     * @return array
     */
    public function findBy($column, $value, $table, $intoClass)
    {
        // Prepare the sql statement
        $statement = $this->pdo->prepare("select * from {$table} WHERE {$column} = ?");
        $statement->execute([$value]);
        $statement->setFetchMode(\PDO::FETCH_CLASS, $intoClass);

        return $statement->fetch();
    }

    /**
     * Function to find row by column and value
     * @param $table
     * @param $column
     * @param $value
     * @param $intoClass
     * @return array
     */
    public function findMultipleBy($column, $value, $table, $intoClass)
    {
        // Prepare the sql statement
        $statement = $this->pdo->prepare("select * from {$table} WHERE {$column} = ?");
        $statement->execute([$value]);
        $statement->setFetchMode(\PDO::FETCH_CLASS, $intoClass);

        return $statement->fetchAll();
    }

    /**
     * Function to insert a row
     * @param $keys
     * @param $placeholders
     * @param $values
     * @param $table
     * @return bool
     */
    public function insert($keys, $placeholders, $values, $table) {
        // Prepare the INSERT SQL statement
        $stmt = $this->pdo->prepare("INSERT INTO {$table} ({$keys}) VALUES ({$placeholders})");

        // Execute the statement and insert our serialized object string
        $exec = $stmt->execute($values);

        // Return the id
        return $exec ? $this->pdo->lastInsertId() : $exec;
    }

    /**
     * Function to delete row
     * @param $id
     * @param $table
     * @return bool
     */
    public function delete($id, $table) {
        // Execute the statement and insert our serialized object string
        $this->pdo->exec("DELETE FROM {$table} WHERE id = ({$id})");

        header('Location: /manager/user/index');
        exit;
    }
}