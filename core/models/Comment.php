<?php
/**
 * Created by PhpStorm.
 * User: safwan
 * Date: 21/10/2018
 * Time: 15:15
 */

namespace core\models;

/**
 * Class Comment
 * @package app\models
 */
class Comment extends BaseModel
{
    /**
     * @var integer
     */
    public $id;

    /**
     * @var string
     */
    public $comment;

    /**
     * @var integer
     */
    public $votes;

    /**
     * @var integer
     */
    public $event_id;

    /**
     * @var integer
     */
    public $care_plan_id;

    /**
     * @var integer
     */
    public $created_by;

    /**
     * Returns table name
     * @return string
     */
    public function getSource()
    {
        return 'comment';
    }

    public function save() {
        $statement =
            $this->pdo->prepare("INSERT INTO comment (comment, votes, event_id, care_plan_id, created_by) 
VALUES(:comment, :votes, :event_id, :care_plan_id, :created_by)");
        $statement->execute(
            [
                'comment' => $this->comment,
                'votes' => $this->votes,
                'event_id' => $this->event_id,
                'care_plan_id' => $this->care_plan_id,
                'created_by'    => $this->created_by
            ]
        );
    }
}