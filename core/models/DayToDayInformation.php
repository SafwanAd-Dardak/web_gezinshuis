<?php
/**
 * Created by PhpStorm.
 * User: safwan
 * Date: 21/10/2018
 * Time: 15:14
 */


namespace core\models;

/**
 * Class DayToDayInformation
 * @package app\models
 */
class DayToDayInformation extends BaseModel
{
    /**
     * @var integer
     */
    public $id;

    /**
     * @var string
     */
    public $date;

    /**
     * @var string
     */
    public $description;

    /**
     * @var string
     */
    public $action;

    /**
     * @var integer
     */
    public $kid_id;

    /**
     * @var integer
     */
    public $owner_id;

    /**
     * Returns table name
     * @return string
     */
    public function getSource()
    {
        return 'day_to_day_information';
    }
}