<?php
/**
 * Created by PhpStorm.
 * User: safwan
 * Date: 20/10/2018
 * Time: 22:13
 */


namespace core\models;

/**
 * Class User
 * @package app\models
 */
class User extends BaseModel
{
    /**
     * @const integer
     */
    const OWNER = 1;

    /**
     * @const integer
     */
    const DOCTOR = 2;

    /**
     * @const integer
     */
    const PARENT = 3;

    /**
     * @const integer
     */
    const KID = 4;

    /**
     * @var integer
     */
    public $id;

    /**
     * @var string
     */
    public $first_name;

    /**
     * @var string
     */
    public $last_name;

    /**
     * @var string
     */
    public $nickname;

    /**
     * @var string
     */
    public $birth_date;

    /**
     * @var string
     */
    public $email;

    /**
     * @var string
     */
    public $password;

    /**
     * @var string
     */
    public $phone_number;

    /**
     * @var string
     */
    public $picture;

    /**
     * @var string
     */
    public $reason;

    /**
     * @var string
     */
    public $proficiency;

    /**
     * @var boolean
     */
    public $active;

    /**
     * @var integer
     */
    public $role;

    /**
     * @var integer
     */
    public $parent_id;

    /**
     * @var integer
     */
    public $care_plan_id;

    /**
     * Returns table name
     * @return string
     */
    public function getSource()
    {
        return 'user';
    }

    /**
     * @param $id
     * @param $password
     * Change password of certain user
     */
    public function changePassword($id, $password) {
        $statement = $this->pdo->prepare("UPDATE user SET password = :password WHERE id = :id");
        $statement->execute(['id' => $id, 'password' => password_hash($password, PASSWORD_BCRYPT)]);
    }
}