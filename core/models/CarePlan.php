<?php
/**
 * Created by PhpStorm.
 * User: safwan
 * Date: 21/10/2018
 * Time: 15:16
 */


namespace core\models;

/**
 * Class CarePlan
 * @package app\models
 */
class CarePlan extends BaseModel
{
    /**
     * @var integer
     */
    public $id;

    /**
     * @var string
     */
    public $schema;

    /**
     * @var string
     */
    public $start;

    /**
     * @var string
     */
    public $review;

    /**
     * @var string
     */
    public $extra;

    /**
     * @var integer
     */
    public $doctor_id;

    /**
     * @var integer
     */
    public $owner_id;

    /**
     * Returns table name
     * @return string
     */
    public function getSource()
    {
        return 'care_plan';
    }

    public function getKid() {
        $user = new User();
        $user = $user->findBy('care_plan_id', $this->id, $user->getSource() ,User::class);
        return $user;
    }
}