<?php
/**
 * Created by PhpStorm.
 * User: safwan
 * Date: 21/10/2018
 * Time: 15:13
 */


namespace core\models;

/**
 * Class Event
 * @package app\models
 */
class Event extends BaseModel
{
    /**
     * @var integer
     */
    public $id;

    /**
     * @var string
     */
    public $date_event;

    /**
     * @var string
     */
    public $event_name;

    /**
     * @var string
     */
    public $picture;

    /**
     * @var integer
     */
    public $creator_id;

    /**
     * Returns table name
     * @return string
     */
    public function getSource()
    {
        return 'event';
    }

    public function getKid() {
        $user = new User();
        $user = $user->findBy('id', $this->creator_id, $user->getSource() ,User::class);
        return $user;
    }

    public function save() {
        $statement =
            $this->pdo->prepare("INSERT INTO event (date_event, event_name, picture, creator_id) 
VALUES(:date_event, :event_name, :picture, :creator_id)");
        $statement->execute(
            [
                'date_event' => $this->date_event,
                'event_name' => $this->event_name,
                'picture' => $this->picture,
                'creator_id' => $this->creator_id,
            ]
        );

        $this->id = $this->pdo->lastInsertId();
    }
}