<?php

/**
 * Return the config for:
 * - Database
 * - Mail
 */
return [
    'database' => [
        'name' => 'adsd_gezinshuis',
        'username' => 'root',
        'password' => 'root',
        'connection' => 'mysql:host=localhost',
        'options' => [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING
        ]
    ],
    'mail' => [
        'email' => 'test@test.nl',
    ]
];
