-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Gegenereerd op: 21 okt 2018 om 13:21
-- Serverversie: 5.7.23
-- PHP-versie: 7.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `adsd_gezinshuis`
--

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `care_plan`
--

CREATE TABLE `care_plan` (
  `id` int(11) NOT NULL,
  `schema` blob,
  `start` date DEFAULT NULL,
  `review` date DEFAULT NULL,
  `extra` text,
  `doctor_id` int(11) NOT NULL,
  `owner_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `comment`
--

CREATE TABLE `comment` (
  `id` int(11) NOT NULL,
  `comment` varchar(255) NOT NULL,
  `votes` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  `care_plan_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `day_to_day_information`
--

CREATE TABLE `day_to_day_information` (
  `id` int(11) NOT NULL,
  `date` date DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `action` varchar(255) NOT NULL,
  `kid_id` int(11) NOT NULL,
  `owner_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `event`
--

CREATE TABLE `event` (
  `id` int(11) NOT NULL,
  `date_event` date DEFAULT NULL,
  `event_name` varchar(255) NOT NULL,
  `picture` varchar(255) DEFAULT NULL,
  `creator_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `nickname` varchar(255) DEFAULT NULL,
  `birth_date` date NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `phone_number` varchar(45) DEFAULT NULL,
  `picture` varchar(255) DEFAULT NULL,
  `reason` text,
  `proficiency` varchar(255) DEFAULT NULL,
  `active` tinyint(4) NOT NULL,
  `role` int(11) NOT NULL COMMENT 'OWNER = 1, DOCTOR = 2, PARENT = 3, KID = 4',
  `parent_id` int(11) DEFAULT NULL,
  `care_plan_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexen voor geëxporteerde tabellen
--

--
-- Indexen voor tabel `care_plan`
--
ALTER TABLE `care_plan`
  ADD PRIMARY KEY (`id`,`doctor_id`,`owner_id`),
  ADD KEY `fk_care_plan_user1_idx` (`doctor_id`),
  ADD KEY `fk_care_plan_user2_idx` (`owner_id`);

--
-- Indexen voor tabel `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`id`,`event_id`,`care_plan_id`),
  ADD KEY `fk_comment_event_idx` (`event_id`),
  ADD KEY `fk_comment_care_plan1_idx` (`care_plan_id`);

--
-- Indexen voor tabel `day_to_day_information`
--
ALTER TABLE `day_to_day_information`
  ADD PRIMARY KEY (`id`,`kid_id`,`owner_id`),
  ADD KEY `fk_user_has_user_user2_idx` (`owner_id`),
  ADD KEY `fk_user_has_user_user1_idx` (`kid_id`);

--
-- Indexen voor tabel `event`
--
ALTER TABLE `event`
  ADD PRIMARY KEY (`id`,`creator_id`),
  ADD KEY `fk_event_user1_idx` (`creator_id`);

--
-- Indexen voor tabel `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email_UNIQUE` (`email`),
  ADD UNIQUE KEY `phone_number_UNIQUE` (`phone_number`),
  ADD KEY `fk_user_user1_idx` (`parent_id`),
  ADD KEY `fk_user_care_plan1_idx` (`care_plan_id`);

--
-- AUTO_INCREMENT voor geëxporteerde tabellen
--

--
-- AUTO_INCREMENT voor een tabel `care_plan`
--
ALTER TABLE `care_plan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT voor een tabel `comment`
--
ALTER TABLE `comment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT voor een tabel `day_to_day_information`
--
ALTER TABLE `day_to_day_information`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT voor een tabel `event`
--
ALTER TABLE `event`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT voor een tabel `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Beperkingen voor geëxporteerde tabellen
--

--
-- Beperkingen voor tabel `care_plan`
--
ALTER TABLE `care_plan`
  ADD CONSTRAINT `fk_care_plan_user1` FOREIGN KEY (`doctor_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_care_plan_user2` FOREIGN KEY (`owner_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Beperkingen voor tabel `comment`
--
ALTER TABLE `comment`
  ADD CONSTRAINT `fk_comment_care_plan1` FOREIGN KEY (`care_plan_id`) REFERENCES `care_plan` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_comment_event` FOREIGN KEY (`event_id`) REFERENCES `event` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Beperkingen voor tabel `day_to_day_information`
--
ALTER TABLE `day_to_day_information`
  ADD CONSTRAINT `fk_user_has_user_user1` FOREIGN KEY (`kid_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_user_has_user_user2` FOREIGN KEY (`owner_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Beperkingen voor tabel `event`
--
ALTER TABLE `event`
  ADD CONSTRAINT `fk_event_user1` FOREIGN KEY (`creator_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Beperkingen voor tabel `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `fk_user_care_plan1` FOREIGN KEY (`care_plan_id`) REFERENCES `care_plan` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_user_user1` FOREIGN KEY (`parent_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
