# ************************************************************
# Sequel Pro SQL dump
# Version 5224
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 8.0.12)
# Database: gezinshuis
# Generation Time: 2018-11-04 04:57:37 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
SET NAMES utf8mb4;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table care_plan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `care_plan`;

CREATE TABLE `care_plan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `schema` blob,
  `start` date DEFAULT NULL,
  `review` date DEFAULT NULL,
  `extra` text,
  `doctor_id` int(11) NOT NULL,
  `owner_id` int(11) NOT NULL,
  PRIMARY KEY (`id`,`doctor_id`,`owner_id`),
  KEY `fk_care_plan_user1_idx` (`doctor_id`),
  KEY `fk_care_plan_user2_idx` (`owner_id`),
  CONSTRAINT `fk_care_plan_user1` FOREIGN KEY (`doctor_id`) REFERENCES `user` (`id`),
  CONSTRAINT `fk_care_plan_user2` FOREIGN KEY (`owner_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table comment
# ------------------------------------------------------------

DROP TABLE IF EXISTS `comment`;

CREATE TABLE `comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `comment` varchar(255) NOT NULL,
  `votes` int(11) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `event_id` int(11) DEFAULT NULL,
  `care_plan_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `created_by` (`created_by`),
  KEY `event_id` (`event_id`),
  KEY `care_plan_id` (`care_plan_id`),
  CONSTRAINT `comment_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`),
  CONSTRAINT `comment_ibfk_2` FOREIGN KEY (`event_id`) REFERENCES `event` (`id`),
  CONSTRAINT `comment_ibfk_3` FOREIGN KEY (`care_plan_id`) REFERENCES `care_plan` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table day_to_day_information
# ------------------------------------------------------------

DROP TABLE IF EXISTS `day_to_day_information`;

CREATE TABLE `day_to_day_information` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` date DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `action` varchar(255) NOT NULL,
  `kid_id` int(11) NOT NULL,
  `owner_id` int(11) NOT NULL,
  PRIMARY KEY (`id`,`kid_id`,`owner_id`),
  KEY `fk_user_has_user_user2_idx` (`owner_id`),
  KEY `fk_user_has_user_user1_idx` (`kid_id`),
  CONSTRAINT `fk_user_has_user_user1` FOREIGN KEY (`kid_id`) REFERENCES `user` (`id`),
  CONSTRAINT `fk_user_has_user_user2` FOREIGN KEY (`owner_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table event
# ------------------------------------------------------------

DROP TABLE IF EXISTS `event`;

CREATE TABLE `event` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date_event` date DEFAULT NULL,
  `event_name` varchar(255) NOT NULL,
  `picture` varchar(255) DEFAULT NULL,
  `creator_id` int(11) NOT NULL,
  PRIMARY KEY (`id`,`creator_id`),
  KEY `fk_event_user1_idx` (`creator_id`),
  CONSTRAINT `fk_event_user1` FOREIGN KEY (`creator_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table user
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `nickname` varchar(255) DEFAULT NULL,
  `birth_date` date NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `phone_number` varchar(45) DEFAULT NULL,
  `picture` varchar(255) DEFAULT NULL,
  `reason` text,
  `proficiency` varchar(255) DEFAULT NULL,
  `active` tinyint(4) NOT NULL,
  `role` int(11) NOT NULL COMMENT 'OWNER = 1, DOCTOR = 2, PARENT = 3, KID = 4',
  `parent_id` int(11) DEFAULT NULL,
  `care_plan_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_UNIQUE` (`email`),
  UNIQUE KEY `phone_number_UNIQUE` (`phone_number`),
  KEY `fk_user_user1_idx` (`parent_id`),
  KEY `fk_user_care_plan1_idx` (`care_plan_id`),
  CONSTRAINT `fk_user_care_plan1` FOREIGN KEY (`care_plan_id`) REFERENCES `care_plan` (`id`),
  CONSTRAINT `fk_user_user1` FOREIGN KEY (`parent_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
