$(document).ready(function(){


    //Bind event to onClick change password button
    $('#changePassword').on('click', function(e){
        e.preventDefault();
        var password = $('#inputPassword');
        var passwordRepeat = $('#inputPasswordRepeat');

        //Remove all error statuses
        password.removeClass('error');
        passwordRepeat.removeClass('error');
        $("#status-text").addClass('hidden');
        $('#status-text').removeClass('success');
        $('#status-text').removeClass('error');


        //Check if password is atleast 5 characters long
        if (password.val().length < 6) {
            password.addClass('error');
            $("#status-text").addClass('error');
            $("#status-text").removeClass('hidden');
            $("#status-text").html('Wachtwoorden moet uit minstens 5 karakters bestaan');
            return;
        }

        //Check if passwords are equal
        if (password.val() != passwordRepeat.val()) {
            password.addClass('error');
            passwordRepeat.addClass('error');
            $("#status-text").addClass('error');
            $("#status-text").removeClass('hidden');
            $("#status-text").html('Wachtwoorden moeten overeenkomen');
            return;
        }

        //Call to change-password to actually change the password
        $.ajax({
            url: "/change-password",
            method: 'POST',
            data: {'password' : password.val() , 'passwordRepeat' : passwordRepeat.val()},
            cache: false,
            success: function(html){
                //TODO: there should be some extra validation here
                $("#status-text").removeClass('hidden');
                $('#status-text').addClass('success');
                $("#status-text").html("Wachtwoord gewijzigd");
            }
        });

        return false;
    });


    if ($('#comments').length != 0) {
        var table = $('#comments').data('table');
        var id = $('#comments').data('id');

        var postForm = '<form method="post" action="/post-comment">';
        postForm += '<input type="hidden" name="table" value="' + table + '">';
        postForm += '<input type="hidden" name="id" value="' + id + '">';
        postForm += '<input type="text" name="comment" id="commentText" placeholder="Commentaar">';
        postForm += '<input type="submit" id="submitComment" name="submit" value="Toevoegen">';
        postForm += '</form>';


        $('#comments').append(postForm);
        var comments = "<h2>Comments</h2>";

        $.ajax({
            url: "/comments?table=" + table + '&id=' + id,
            method: 'GET',
            dataType: 'json',
            cache: false,
            success: function(json){
                console.log(json);
                $.each(json, function(index,value){
                    comments += '<div>';
                    comments += '<p>' + value.comment + '</p>';
                    comments += '</div><hr>';
                });

                $('#comments').append(comments);
            }
        });

        $(document).on('click', '#submitComment', function(event){
            event.preventDefault();

            $.ajax({
                url: "/post-comment?table=" + table + '&id=' + id,
                method: 'POST',
                data: {'table' : table, 'id' : id, 'comment' : $('#commentText').val()},
                success: function(json){
                    console.log(json);
                }
            });

            return false;
        });






    }
});