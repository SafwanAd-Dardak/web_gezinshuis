function deleteRow(name, title, url, id){
    swal({
        title: "Weet u het zeker?",
        text: "De " + name + " '" + title + "' zal niet meer herstelbaar zijn!",
        icon: "warning",
        buttons: [
            "Annuleer",
            "Ja"
        ],
        dangerMode: true,
    }).then((willDelete) => {
        if (willDelete) {
            var _token = $('input[name="_token"]').val();
            $.ajax({
                type: "DELETE",
                url: url + id,
                data: { _token : _token },
                success: function(data, textStatus){
                    swal("De " + name + " is succesvol verwijderd!", {
                        icon: "success"
                    });
                    deleteRowOfTable(id);
                },
                error: function(xhr, textStatus, errorThrown){
                    swal("Er is iets fout gegaan!", "De " + name + " '" + title + " kon niet worden verwijderd'!", "warning");
                }
            });
        } else {
            swal("De " + name + " zal niet worden verwijderd!");
}
});
}

// Function to delete row of table
function deleteRowOfTable(id) {
    var table = $('#dataTable').DataTable();
    table.row( $('#overviewRow' + id) ).remove().draw();
}